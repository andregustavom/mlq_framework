source("./load_libraries.r")

#' Run experiment varying test set size and class distribution for a given dataset for Quantification Tree
#' 
#' Exprimental setup for the IJCAI'20 paper: Importance of the Test Set Size in Quantification Assessment. This function run a system call for 
#' quantification tree software available from https://kdd.isti.cnr.it/homes/monreale/software/
#' @param dts_id an integer value range from 1 to 13, indicating each dataset used in the experiments
#' @param counter quantification method. For instance, MS, HDy-LP, DyS-PS, SORD, CC, PCC, ACC, MLQ, etc.
#' @param score data.frame containing the scores estimated on training set, with three columns (1. scores to be positive; 2. scores to be negative; 3. actual class)
#' @return The class distribution in the test set
#' @export

run_experiment_QT <- function(dts_id, counter){
  
  dts          <- list.files("./datasets/")[dts_id]
  thr          <- 0.5 # classifier threshold
  
  print(paste0("Quantifying ----->>>> ",dts))
  db_files  <- paste0("./models_train_test/classifier_RF_QT_", dts)
  train     <- readRDS(paste0("./models_train_test/train_", dts, ".rds"))
  test      <- readRDS(paste0("./models_train_test/test_", dts, ".rds"))
  classifier<- paste0("classifier_RF_QT_", dts)
  
  if(!dir.exists("./test_QT/")) dir.create("./test_QT/")
  
  if(!file.exists(db_files)){
    train$class <- as.factor(train$class)
    nn <- names(train)
    nn <- c(nn[-1], nn[1])
    train <- cbind(train[,-1], train[,1])
    names(train) <- nn
    
    write.arff(train, paste0("./models_train_test/train_arff_", dts, ".arff"))
    
    command <- paste0("java -Xmx6G -cp quantify.jar:weka.jar:. weka.classifiers.trees.RandomForest -I 200 -P 1 -t ", "./models_train_test/train_arff_", dts, 
                      ".arff", " -d ./models_train_test/", classifier)
    system(command)
  }
  
  print(paste0("------>>>> Starting ", counter, " quantifier"))
  ran_f <- paste0("./results/QT_results_",counter, "_",dts,".rds")    
  if(!file.exists(ran_f)){
    print(paste0("Running ", counter))                
    re <- run_qntMethod_over_dataset_QT(train = NA, 
                                     test = test, 
                                     qntMethod = counter, 
                                     classifier = classifier, 
                                     TprFpr = NA, 
                                     scores = NA, 
                                     dts = dts, 
                                     thr = thr, 
                                     measure = -1)
    re$dataset <- dts
    saveRDS(re, paste0("./results/QT_results_",counter, "_",dts,".rds")) 
    
  }else{  
    print(paste0("You have already performed the experimental setup comprised of ", dts ," dataset and ", counter," quantifier."))
    print(paste0("Please, check this file: ",ran_f))
    }
  
  return(1)
  
}

#Uncomment for running using Quantification Trees.
#run_experiment_QT(dts_id=1, counter="QT")
#run_experiment_QT(dts_id=2, counter="QT")
#run_experiment_QT(dts_id=3, counter="QT")
#run_experiment_QT(dts_id=4, counter="QT")
#run_experiment_QT(dts_id=5, counter="QT")
#run_experiment_QT(dts_id=6, counter="QT")
#run_experiment_QT(dts_id=7, counter="QT")
#run_experiment_QT(dts_id=8, counter="QT")
#run_experiment_QT(dts_id=9, counter="QT")
#run_experiment_QT(dts_id=10, counter="QT")
#run_experiment_QT(dts_id=11, counter="QT")
#run_experiment_QT(dts_id=12, counter="QT")
#run_experiment_QT(dts_id=13, counter="QT")



