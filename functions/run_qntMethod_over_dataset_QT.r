#' Function used to perform experiments, given a quantifier method, varyng the test size and the class distribution
#' 
#' Given a quantifier method (qntMethod), a classifier, and a test set, that function extract from the test set several 
#' subsets varying the size and for each size the class distribution is also varied
#' @author Andre G Maletzke - andregustavom at gmail.com
#' @param train data.frame containing the training set
#' @param test data.frame containing the test set
#' @param qntMethod the quantifier algorithm name
#' @param classifier A classifier built on a training set
#' @param TprFpr data.frame composed by True Positive and False Positive rates estimated on training set
#' @param scores data.frame contaning the scores for each instance of the training set, estimated by 10-folds
#' @param dts dataset name
#' @param thr threshold used for some quantifier methos such as CC
#' @param measure parameter of the DyS framework
#' @return The class distribution for each simulated subset extracted from the test set
#' @export

run_qntMethod_over_dataset_QT <- function(train, test, qntMethod, classifier, TprFpr, scores, dts, thr=0.5, measure="hellinger"){
  
  
  var_perc <- seq(0,1,0.01)                       # range of values used to evaluate the impact of class distribution on error
  var_size <- c(seq(10,100,10), seq(200,500,100)) # range of test set sizes
  n_tests  <- 10                                  # for each combination of var_perc and var_size we build n_tests aleatory scenarios
  
  resultsALL     <- NULL
  
  for(k in 1:length(var_size)){ 
    print(paste0("Test size ", var_size[k])) 
    results <- NULL   
    for(i in 1:length(var_perc)){    
      for(j in 1:n_tests){
        test_info <- getBatch(test, c(1, 2),  c(var_perc[i], 1 - var_perc[i]), var_size[k])            
        test_set <- test_info[[1]]            
        freq_REAL <- round(table(test_set$class)/sum(table(test_set$class)),2)
        test_set$class <- as.factor(test_set$class)
        levels(test_set$class) <- c("1", "2")
        nn <- names(test_set)
        nn <- c(nn[-1], nn[1])
        test_set <- cbind(test_set[,-1], test_set[,1])
        names(test_set) <- nn
        
        write.arff(test_set, paste0("./test_QT/test_set_", dts,".arff"))
        
        command <- paste0("java  -Xmx5G -cp quantify.jar:weka.jar:. weka.classifiers.trees.RandomForest -l ./models_train_test/", classifier,  
                          " -T ", "./test_QT/test_set_", dts,".arff")
        
        system(paste0(command,"> ", dts,"_re.txt"))
        
        x <- read.delim(paste0(dts,"_re.txt"))
        
        pos <- as.vector(na.omit(as.numeric(strsplit(as.character(x[4,])," ")[[1]])))[1]
        
        pos <- pos + as.vector(na.omit(as.numeric(strsplit(as.character(x[5,])," ")[[1]])))[1]
        
        pos <- pos/nrow(test_set)
        
        freq_PRE <- as.data.frame(round(cbind(pos, 1-pos),2))
        names(freq_PRE) <- c("1", "2")

        results  <- rbind(results, unlist(c("RF",
                                            1,
                                            freq_REAL,
                                            freq_PRE,
                                            round(abs(freq_REAL[1]-freq_PRE[1]),2),
                                            nrow(test_set), 
                                            -1,
                                            -1,
                                            0,
                                            "QT",
                                            thr                                              
        )))
         
      }
    }

    resultsALL <- rbind(resultsALL, results)
    
  }  
  resultsALL <- as.data.frame(resultsALL)
  names(resultsALL) <- c("Alg", 
                      "ID_positiveLabel",
                      paste("R", as.character(1:2), sep="_"), 
                      paste("P", as.character(1:2), sep="_"), 
                      "MAE-C1",
                      "Test_Size",
                      "nBins",
                      "Distance",
                      "Value.dist",
                      "Qnt",
                      "Threshold"
                      
  )
  return(resultsALL)
}


