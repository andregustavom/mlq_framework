
batch_kurtosis <- function(batch){
	v_kurtosis <- NULL
	
	for(i in 1:ncol(batch)){
		aux <- kurtosis(batch[,i])
		if(!is.na(aux))
			v_kurtosis <- c(v_kurtosis, aux)
	}
	return(mean(v_kurtosis))
}

batch_correlation <- function(batch){

	v_cor <- NULL
	attr_mean <- rowMeans(batch)
	for(i in 1:ncol(batch)){
		aux <- cor(batch[,i], attr_mean)
		if(!is.na(aux))
			v_cor <- c(v_cor, aux)
	}
	return(mean(v_cor))
}

batch_skewness <- function(batch){
  v_skewness <- NULL
  for(i in 1:ncol(batch)){
    aux <- skewness(batch[,i])
    if(!is.na(aux))
      v_skewness <- c(v_skewness, aux)
  }
  return(mean(v_skewness))
}


batch_coefVar <- function(batch){
	v_coefVar <- NULL
	for(i in 1:ncol(batch)){
		aux <- sd(batch[,i]) / mean(batch[,i])
		if(!is.na(aux) && aux != Inf)
			v_coefVar <- c(v_coefVar, aux)
	}
	return(mean(v_coefVar))
}
