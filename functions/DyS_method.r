#' Quantify a set of events by DyS framework
#' 
#' Quantify a set of events given based on a fitted Weka classifier applying the Expectation Maximization for Quantification (EMQ) method
#' @param ts A unlabeled set
#' @param classifier A Weka classifier built on a training set
#' @param score data.frame containing the scores estimated on training set, with three columns (1. scores to be positive; 2. scores to be negative; 3. actual class)
#' @param measure measure used to compare the mixture histogram against the histogram obtained from the test set
#' @return The class distribution in the test set
#' @export

DyS_method <- function(ts, classifier, score, measure="hellinger"){
  
  alpha <- seq(0,1,by=0.01)
  
  idx_pos <- which(score[,3]==1)
  idx_neg <- which(score[,3]==2)
  
  sc_1 <- score[idx_pos,1]
  sc_2 <- score[idx_neg,1]
  
  #Usc <- applyModel(classifier, ts, T)
  Usc <- predict(classifier, ts, type = c("prob"))

  b_sizes <- c(seq(2,20,2),30)

  result <- NULL
  
  for(hi in 1:length(b_sizes)){
    
    hists <- getHist(sc_1, sc_2, Usc[,1], measure, b_sizes[hi])
    
    Sty_1 <- hists[[1]]
    Sty_2 <- hists[[2]]
    Uy    <- hists[[3]]
    
    vDist <- NULL
    vDistAll <- NULL
    f <- function(x){
      return(distance_DYS(rbind((Sty_1*x)+ (Sty_2*(1-x)), Uy), method = measure, p=0.5))
    }
    
    result <- c(result, TernarySearch(0, 1, f))
    vDistAll <- c(vDistAll, f(result))

  } 
    
  result <- median(result)
  result <- c(result, 1 - result)
  names(result) <- c("1", "2")
  return(list(round(result,2),vDistAll[order(vDistAll)[1]]))
  
}