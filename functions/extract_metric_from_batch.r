extract_metric_from_batch <- function(bi){
  
  assMean <- batch_skewness(bi)
  kurMean <- batch_kurtosis(bi)
  corMean <- batch_correlation(bi)
  coeMean <- batch_coefVar(bi)
  
  return(round(c(assMean, kurMean, corMean, coeMean),2))
  
}