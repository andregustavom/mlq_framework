

load_train <- function(path="./results_learning/"){
  vf <- list.files(path)
  re <- NULL
  for(i in vf){
    x <- readRDS(paste0(path, i))
    re <- rbind(re,x)
  }
  return(re)
}