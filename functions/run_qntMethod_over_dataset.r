#' Function used to perform experiments, given a quantifier method, varyng the test size and the class distribution
#' 
#' Given a quantifier method (qntMethod), a classifier, and a test set, that function extract from the test set several 
#' subsets varying the size and for each size the class distribution is also varied
#' @author Andre G Maletzke - andregustavom at gmail.com
#' @param train data.frame containing the training set
#' @param test data.frame containing the test set
#' @param qntMethod the quantifier algorithm name
#' @param classifier A classifier built on a training set
#' @param TprFpr data.frame composed by True Positive and False Positive rates estimated on training set
#' @param scores data.frame contaning the scores for each instance of the training set, estimated by 10-folds
#' @param dts dataset name
#' @return The class distribution for each simulated subset extracted from the test set
#' @export

run_qntMethod_over_dataset <- function(train, 
                                       test, 
                                       qntMethod, 
                                       classifier, 
                                       TprFpr, 
                                       scores, 
                                       dts){

  
  var_perc <- seq(0,1,0.01)                       # range of values used to evaluate the impact of class distribution on error
  var_size <- c(seq(10,100,10), seq(200,500,100)) # range of test set sizes
  n_tests  <- 10                                  # for each combination of var_perc and var_size we build n_tests aleatory scenarios
  thr      <- 0.5
  
  results     <- NULL
  QntRun      <- qntMethod
  
  
  vdist <- c("topsoe", "jensen_difference", "prob_symm", "ord")
  names(vdist) <- c("TS", "JD", "PS", "ORD")
  
  measure <- NA
  qi <- qntMethod
  aux <- strsplit(qntMethod, "DyS-")[[1]]
  if(length(aux)>1 ){
    qi <- "DyS"
    measure   <- vdist[aux[2]]
  }

  #Learning the recomender. The process of features extraction was performed by 
  if(QntRun=="MLQ"){  
    re<- load_train("./results_learning/")
    x <- re[,-c(5:9)]
    x <- as.data.frame(apply(x[,-7], 2, as.numeric))
    x <- cbind(x, as.vector(re$class))
    nn<- names(x)
    nn[length(nn)] <- "class"
    names(x) <- nn
    x$class <- as.factor(x$class)
    modelCounter <- randomForest(class~., data=x, ntree = 200)
    print(paste0("Possible Quantifiers --> ", unique(x$class)))
  }

  if(!dir.exists(paste0("./tmp_", QntRun))) dir.create(paste0("./tmp_", QntRun))
  if(!dir.exists(paste0("./tmp_",QntRun,"/", dts))) dir.create(paste0("./tmp_",QntRun,"/", dts))
  
  for(k in 1:length(var_size)){ 
    if(file.exists(paste0("./tmp_",QntRun,"/", dts, "/", var_size[k], ".rds"))){
        print(paste0("Load data of size", var_size[k]))
        auxre   <- readRDS(paste0("./tmp_",QntRun,"/", dts, "/", var_size[k], ".rds"))
        results <- rbind(results, auxre)
    }else{
        print(paste0("Test size ", var_size[k]))    
        for(i in 1:length(var_perc)){      
          for(j in 1:n_tests){
                test_info <- getBatch(test, c(1, 2),  c(var_perc[i], 1 - var_perc[i]), var_size[k])            
                test_set  <- test_info[[1]]            
                freq_REAL <- round(table(test_set$class)/sum(table(test_set$class)),2)
                predTest  <- predict(classifier, test_set, type = c("prob"))
                if(qi%in%c("PCC", "PACC")){
                  calib       <- calibrate(as.factor(scores[,3]), as.numeric(scores[,1]), class1=1, method="isoReg",assumeProbabilities=TRUE)
                  predTest[,1]<- applyCalibration(as.numeric(predTest[,1]), calib)
                }
                
                # To run the meta-learning proposal (MLQ) of the paper Importance of the Test Set Size in Quantification Assessment (IJCAI'20)
                if(QntRun=="MLQ"){
                  cmplM <- extract_metric_from_batch(test_set[,-1])
                  dys_result <- DyS_method(ts = test_set, 
                                classifier = classifier, score= scores,measure = "topsoe")
                  
                  dys_dist   <- round(dys_result[[2]],3)
                  ex <- as.data.frame(t(c(cmplM, nrow(test_set), dys_dist)))
                  names(ex) <- c("C1", "C2", "C3", "C4", "Size", "dist")
                  qi <- as.vector(predict(modelCounter, ex))
                  ifelse(strsplit(qi, "DyS-")[[1]][1]=="", qntMethod <- "DyS", qntMethod <- qi)
                  measure <- vdist[strsplit(qi,"-")[[1]][2]]
                }
                
                # To run the baseline (RND). Figure 8 of the paper: The Importance of the Test Set Size in Quantification Assessment (IJCAI'20)
                if(QntRun=="RND"){
                  measure   <- "topsoe"
                  qntMethod <- sample(c("MS", "SORD", "DyS"),1)

                }
                # To run the topline (TOP). Figure 8 of the paper: The Importance of the Test Set Size in Quantification Assessment (IJCAI'20)
                if(QntRun=="TOP"){
                  top_qnt <- as.data.frame(t(c(10,10,10)))
                  names(top_qnt) <- c('MS', "DyS", "SORD")
                  top_dst <- matrix(ncol=2, nrow=3)
                  measure <- "topsoe"
                  qntMethod  <- "MS"
                  qnt_re_MS  <- apply.qntMethod(qntMethod, 
                                                p.score = scores[scores[,3]==1,1],
                                                n.score = scores[scores[,3]==2,1], 
                                                   test = predTest[,1], 
                                                 TprFpr = apply(TprFpr,2,as.numeric),
                                                    thr = thr,
                                                measure = measure,
                                                  train = train)
                  freq_PRE   <- qnt_re_MS[[1]]
                  top_qnt[1] <- round(abs(freq_REAL[1]-freq_PRE[1]),2)
                  top_dst[1,]<- as.numeric(freq_PRE)

                  if(!top_qnt[1]==0){
                    qntMethod  <- "DyS"
                    qnt_re_DyS <- apply.qntMethod(qntMethod, 
                                                  p.score = scores[scores[,3]==1,1],
                                                  n.score = scores[scores[,3]==2,1], 
                                                     test = predTest[,1], 
                                                   TprFpr = apply(TprFpr,2,as.numeric),
                                                      thr = thr,
                                                  measure = measure,
                                                    train = train)
                    freq_PRE   <- qnt_re_DyS[[1]]
                    top_qnt[2] <- round(abs(freq_REAL[1]-freq_PRE[1]),2)
                    top_dst[2,]<- as.numeric(freq_PRE)

                    if(!top_qnt[2]==0){
                      qntMethod   <- "SORD"
                      qnt_re_SORD <- apply.qntMethod(qntMethod, 
                                                     p.score = scores[scores[,3]==1,1],
                                                     n.score = scores[scores[,3]==2,1], 
                                                        test = predTest[,1], 
                                                      TprFpr = apply(TprFpr,2,as.numeric),
                                                         thr = thr,
                                                     measure = measure,
                                                       train = train)
                      freq_PRE    <- qnt_re_SORD[[1]]
                      top_dst[3,] <- as.numeric(freq_PRE)
                    }
                  }
                  
                  qntMethod <- names(top_qnt)[min(which.min(top_qnt))]
                  freq_PRE  <- top_dst[min(which.min(top_qnt)),]

                }else{
                  qnt_re <- apply.qntMethod(qi,
                                            p.score = scores[scores[,3]==1,1],
                                            n.score = scores[scores[,3]==2,1], 
                                               test = predTest[,1], 
                                             TprFpr = apply(TprFpr,2,as.numeric), 
                                                thr = thr,
                                            measure = measure,
                                              train = train)
                  freq_PRE <- round(qnt_re,2)
                }                
                
                if(QntRun=="MLQ") measure <- "NA"
                if(QntRun=="RND") measure <- "NA"
                results  <- rbind(results, unlist(c("RF",
                                                   1,
                                                   freq_REAL,
                                                   freq_PRE,
                                                   round(abs(freq_REAL[1]-freq_PRE[1]),2),
                                                   nrow(test_set), 
                                                   qntMethod,
                                                   measure,
                                                   0,
                                                   QntRun,
                                                   thr                                              
                )))
                
          }
        }
      }
    saveRDS(results[which(results[,8]==var_size[k]),], paste0("./tmp_",QntRun,"/", dts, "/", var_size[k], ".rds"))
  }  
  results <- as.data.frame(results)
  names(results) <- c("Alg", 
                      "ID_positiveLabel",
                      paste("R", as.character(1:2), sep="_"), 
                      paste("P", as.character(1:2), sep="_"), 
                      "MAE-C1",
                      "Test_Size",
                      "nBins",
                      "Distance",
                      "Value.dist",
                      "Qnt",
                      "Threshold"
                      
  )
  return(results)
}


