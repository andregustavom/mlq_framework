source("./load_libraries.r")
#library("batch")
#parseCommandArgs()

learning_size_rule <- function(dts_id){
  
  ml_alg_cl    <- "RF" # machine learning algorithm used to induce the classifier for each dataset
  dts          <- list.files("./datasets/")[dts_id]
  
  var_size    <- c(10,20,30,40,50,100,200,300,400,500) # range of test set sizes
  n_tests     <- 100                           # for each combination of var_perc and var_size we build n_tests aleatory scenarios
  
  results     <- NULL
  reFeatures  <- NULL
  
  vdist       <- c("topsoe", "jensen_difference", "prob_symm", "ord", "sord")
  names(vdist)<- c("TS", "JD", "PS", "ORD", "SORD")
  counters    <- c("MS","SORD", "DyS-PS")
  
  
  print(paste0("Quantifying ----->>>> ",dts))
  db_files <- paste0("./models_train_test/classifier_RF_", dts, ".rds")
  
  
  # load the .RDS file of each dataset
  db        <- readRDS(paste0("./models_train_test/train_", dts, ".rds"))

  if(dts=="qg.data"){
    db$author <- as.integer(as.factor(db$author))
    db$class <- as.factor(db$class)
  }
  if(dts=="winequality.data"){
    db$quality <- as.integer(as.factor(db$quality))
    db$class <- as.factor(db$class)
  }
  
  folds <- createFolds(db$class, k=3)
  train <- db[c(folds[[1]], folds[[2]]),]#db[hdl$tr,]
  test  <- db[folds[[3]],]#db[hdl$ts,]
  
  tryCatch(classifier <- randomForest(class~., data=train, ntree = 200), error = function(e) { print("ERROR 1 - Classifier error!")})
  tryCatch(scores     <- getScore_using_K_folds(train, ml_alg_cl, 10), error = function(e) { print("ERROR 2 - Scores error!")})
  tryCatch(TprFpr     <- getTPRandFPRbyThreshold(scores), error = function(e) { print("ERROR 3 - TprFpr error!")})

  for(k in 1:length(var_size)){ 
    print(paste0("Test size ", var_size[k]))    
      for(j in 1:n_tests){
        per <- sample(seq(0,1,0.01),1)
        test_info <- getBatch(test, c(1, 2),  c(per, 1 - per), var_size[k])            
        test_set <- test_info[[1]]            
        freq_REAL <- round(table(test_set$class)/sum(table(test_set$class)),2)
        predTest  <- predict(classifier, test_set, type = c("prob"))
        
        for(qi in counters){
          qntMethod <- qi
          if(qi!="HDy-LP"){
            ifelse(length(strsplit(qi, "-")[[1]]) > 1, qntMethod <- strsplit(qi, "-")[[1]][1], qntMethod <- qi)
            nk <- as.numeric(strsplit(qntMethod, "_")[[1]][1])
            if(is.na(nk)){
              nk <- 1
            }
          }
          
          qnt_re <- apply.qntMethod(qntMethod,
                                    p.score = scores[scores[,3]==1,1],
                                    n.score = scores[scores[,3]==2,1], 
                                    test = predTest[,1], 
                                    TprFpr = apply(TprFpr,2,as.numeric), 
                                    thr = 0.5,
                                    measure = vdist[strsplit(qi,"-")[[1]][2]],
                                    train = train)
          
          freq_PRE <- qnt_re[[1]]
          results  <- c(results, as.numeric(round(abs(freq_REAL[1]-freq_PRE[1]),2)))
        }
        
        xra   <- min(which(results==min(results)))
        bQnt  <- counters[xra]
        cmplM <- extract_metric_from_batch(test_set[,-1])
        dys_result <- DyS_method(ts = test_set, 
                                 classifier = classifier, score= scores,measure = "topsoe")
        dys_dist   <- round(dys_result[[2]],3)
        reFeatures <- rbind(reFeatures, c( cmplM,var_size[k], dys_dist, bQnt))
        results    <- NULL
      }
  } 
  reFeatures        <- as.data.frame(reFeatures)
  names(reFeatures) <- c("C1", "C2", "C3", "C4", "Size","dist" ,"class")

  saveRDS(reFeatures, paste0("./results_learning/results_", dts,".rds")) 
  return(1)
  
}


#dts_id is the number of each dataset from the dataset directory
#learning_size_rule(dts_id=1)
#learning_size_rule(dts_id=2)
#learning_size_rule(dts_id=3)
#learning_size_rule(dts_id=4)
#learning_size_rule(dts_id=5)
#learning_size_rule(dts_id=6)
#learning_size_rule(dts_id=7)
#learning_size_rule(dts_id=8)
#learning_size_rule(dts_id=9)
#learning_size_rule(dts_id=10)
#learning_size_rule(dts_id=11)
#learning_size_rule(dts_id=12)
#learning_size_rule(dts_id=13)

