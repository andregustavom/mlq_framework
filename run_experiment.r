source("./load_libraries.r")

#' Run experiment varying test set size and class distribution for a given dataset
#' 
#' Exprimental setup for the IJCAI'20 paper: Importance of the Test Set Size in Quantification Assessment
#' @param dts_id an integer value range from 1 to 13, indicating each dataset used in the experiments
#' @param counter quantification method. For instance, MS, HDy-LP, DyS-PS, SORD, CC, PCC, ACC, MLQ, etc.
#' @param score data.frame containing the scores estimated on training set, with three columns (1. scores to be positive; 2. scores to be negative; 3. actual class)
#' @return The class distribution in the test set
#' @export

run_experiment <- function(dts_id, counter){
  
  ml_alg_cl    <- "RF" # machine learning algorithm used to induce the classifier for each dataset
  dts          <- list.files("./datasets/")[dts_id]
  
  print(paste0("Quantifying ----->>>> ",dts))
  db_files <- paste0("./models_train_test/classifier_RF_", dts, ".rds")
    
  if(!file.exists(db_files)){
    # Training - 50% and Test - 50%
    db          <- as.data.frame(fread(paste0("./datasets/",dts)))
    
    if(dts=="qg.data")
      db$author <- as.integer(as.factor(db$author))
    if(dts=="winequality.data")
      db$quality <- as.integer(as.factor(db$quality))

    db          <- db %>% mutate_if(is.character, as.factor)      
    hdl         <- holdout(db$class, ratio = 0.5)
    train       <- db[hdl$tr,]
    test        <- db[hdl$ts,]
    
    tryCatch(classifier <- randomForest(class~., data=train, ntree = 200), error = function(e) { print("ERROR 1 - Classifier error!")})
    tryCatch(scores     <- getScore_using_K_folds(train, 10), error = function(e) { print("ERROR 2 - Scores error!")})
    tryCatch(TprFpr     <- getTPRandFPRbyThreshold(scores), error = function(e) { print("ERROR 3 - TprFpr error!")})
    
    # save in .RDS files classifier, training set, test set, scores and TprFpr for each dataset
    saveRDS(classifier, paste0("./models_train_test/classifier_RF_", dts, ".rds"))
    saveRDS(train     , paste0("./models_train_test/train_", dts, ".rds"))
    saveRDS(test      , paste0("./models_train_test/test_", dts, ".rds"))
    saveRDS(scores    , paste0("./models_train_test/scores_", dts, ".rds"))
    saveRDS(TprFpr    , paste0("./models_train_test/TprFpr_", dts, ".rds"))
  }else{
      # load the .RDS file of each dataset
      train     <- readRDS(paste0("./models_train_test/train_", dts, ".rds"))
      test      <- readRDS(paste0("./models_train_test/test_", dts, ".rds"))
      scores    <- readRDS(paste0("./models_train_test/scores_", dts, ".rds"))
      TprFpr    <- readRDS(paste0("./models_train_test/TprFpr_", dts, ".rds"))
      classifier<- readRDS(paste0("./models_train_test/classifier_RF_", dts, ".rds"))

      if(dts=="qg.data"){
        train$author <- as.integer(as.factor(train$author))
        test$author <- as.integer(as.factor(test$author))
        train$class <- as.factor(train$class)
        test$class <- as.factor(test$class)
        tryCatch(classifier <- randomForest(class~., data=train, ntree = 200), error = function(e) { print("ERROR 1 - Classifier error!")})
        tryCatch(scores     <- getScore_using_K_folds(train, 10), error = function(e) { print("ERROR 2 - Scores error!")})
        tryCatch(TprFpr     <- getTPRandFPRbyThreshold(scores), error = function(e) { print("ERROR 3 - TprFpr error!")})          
      }
      if(dts=="winequality.data"){
        train$quality <- as.integer(as.factor(train$quality))
        test$quality <- as.integer(as.factor(test$quality))
        train$class <- as.factor(train$class)
        test$class <- as.factor(test$class)
        tryCatch(classifier <- randomForest(class~., data=train, ntree = 200), error = function(e) { print("ERROR 1 - Classifier error!")})
        tryCatch(scores     <- getScore_using_K_folds(train, 10), error = function(e) { print("ERROR 2 - Scores error!")})
        tryCatch(TprFpr     <- getTPRandFPRbyThreshold(scores), error = function(e) { print("ERROR 3 - TprFpr error!")})          
      }
    }

    db_files <- paste0("./models_train_test/sampled_scores_", dts, ".rds")
    # To reduce the time of experiments we use only a sample of the positive and negative scores. This step has only efect over DyS and HDy 
    if(length(which(counter%in%c("DyS-ORD", "MLQ", "RND", "SORD", "TOP")))>0 ){
      if(!file.exists(db_files)){
        ifelse(length(which(scores[,3]==1))<1000 , p_scores <- scores[which(scores[,3]==1),], p_scores <- scores[sample(which(scores[,3]==1),1000),])
        ifelse(length(which(scores[,3]==2))<1000 , n_scores <- scores[which(scores[,3]==2),], n_scores <- scores[sample(which(scores[,3]==2),1000),])
        scores   <- rbind(p_scores, n_scores)
        saveRDS(scores    , paste0("./models_train_test/sampled_scores_", dts, ".rds"))
      }else{
          scores    <- readRDS(paste0("./models_train_test/sampled_scores_", dts, ".rds"))
          print("2000 scores loaded!")
        }
        }else{
          print("All scores used!")
        }

  if(!dir.exists(paste0("./results_", counter)))  
    dir.create(paste0("./results_", counter))

  print(paste0("------>>>> Starting ", counter, " quantifier"))
  ran_f <- paste0("./results_", counter,"/results_",counter, "_",dts, ".rds")    
  if(!file.exists(ran_f)){
      print(paste0("Running ", counter))                
      re <- run_qntMethod_over_dataset(train = train, 
                                        test = test, 
                                   qntMethod = counter, 
                                  classifier = classifier, 
                                      TprFpr = TprFpr, 
                                      scores = scores, 
                                         dts = dts)
      re$dataset <- dts
      saveRDS(re, paste0("./results_", counter,"/results_",counter, "_",dts,".rds")) 
  
  }else{  
    print(paste0("You have already performed the experimental setup comprised of ", dts ," dataset and ", counter," quantifier."))
    print(paste0("Please, check this file: ",ran_f))
  }
  
  print(paste0("The results were saved in ./results_", counter,"/results_",counter, "_",dts,".rds"))
  return(1)
}

#uncomment for running.
#run_experiment(dts_id=1, counter="MLQ")
#run_experiment(dts_id=2, counter="MLQ")
#run_experiment(dts_id=3, counter="MLQ")
#run_experiment(dts_id=4, counter="MLQ")
#run_experiment(dts_id=5, counter="MLQ")
#run_experiment(dts_id=6, counter="MLQ")
#run_experiment(dts_id=7, counter="MLQ")
#run_experiment(dts_id=8, counter="MLQ")
#run_experiment(dts_id=9, counter="MLQ")
#run_experiment(dts_id=10, counter="MLQ")
#run_experiment(dts_id=11, counter="MLQ")
#run_experiment(dts_id=12, counter="MLQ")
#run_experiment(dts_id=13, counter="MLQ")


